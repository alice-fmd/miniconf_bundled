#! /usr/bin/rpm
%define _unpackaged_files_terminate_build  0
%define need_date         0
%define debug_package     %{nil}
%define rel_post          %{nil}

%if %{?variant:1} %{!?variant:0}
%define rel_post          _%{variant}
%define is_variant        1
%define make_args         VARIANT=%{variant}

%if %{variant} == "daq" 
%define need_date         1
%endif

%else 
%define make_args         %{nil}
%endif 


# --------------------------------------------------------------------
Summary: 	@PACKAGE_NAME@
Name: 		@PACKAGE@
Version: 	@RPM_VERSION@
Release: 	@RELEASE@%{rel_post}
License: 	GPL
Vendor:		Niels Bohr Institute
Packager: 	Christian Holm Christensen <cholm@nbi.dk>
Group: 		Applications/Alice
URL: 		http://fmd.nbi.dk/fmd/fee/@PACKAGE@
Source:		@PACKAGE@_bundled-@VERSION@.tar.gz
BuildRoot: 	%{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix:		%{_peddir}
AutoReqProv: 	yes
%if %{need_date}
BuildRequires:	date
%endif
# %if %{?_vendor}
# %if %{_vendor} == "MandrakeSoft"
# BuildRequires: MySQL-devel >= 4.1.0
# %else
# BuildRequires: mysql-devel >= 4.1.0
# %endif
# %else
# BuildRequires: mysql-devel >= 4.1.0
# %endif
# BuildRequires:  gsl-devel
%description
@PACKAGE_NAME@

# --------------------------------------------------------------------
%files
%defattr(-,root,root)
%{_bindir}/*
%{_sbindir}/*
%{_mandir}/man1/*
%{_mandir}/man8/*
%config(noreplace) %attr(644,fmd,fmd) %{_sysconfdir}/sysconfig/%{name}d*
%{_sysconfdir}/init.d/*
%attr(755,fmd,fmd) /var/log/%{name}d
%attr(755,fmd,fmd) /var/run/%{name}d


# --------------------------------------------------------------------
%prep 
%setup -q -n @PACKAGE@_bundled-@VERSION@


# --------------------------------------------------------------------
%build
%if %{need_date}
. /date/setup.sh
%endif
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT %{make_args}

# --------------------------------------------------------------------
%install
# Fix up configuration scripts 
%if %{need_date}
sed -e 's/^BASE=.*/BASE=LDCCONF/' \
    -e '/^FEES=/,+2 d' \
    -e 's/^DIM_DNS_NODE=.*/DIM_DNS_NODE=aldaqecs/' \
    -e 's/^# *FEES=.*"/FEES=rorc/' \
    < $RPM_BUILD_ROOT/etc/sysconfig/%{name}d \
    > $RPM_BUILD_ROOT/etc/sysconfig/%{name}d.tmp
mv $RPM_BUILD_ROOT/etc/sysconfig/%{name}d.tmp \
    $RPM_BUILD_ROOT/etc/sysconfig/%{name}d
%endif

mkdir -p $RPM_BUILD_ROOT%{_var}/log/%{name}d
mkdir -p $RPM_BUILD_ROOT%{_var}/run/%{name}d
export STRIP=/bin/true
export OBJDUMP=/bin/true

# --------------------------------------------------------------------
%clean
make distclean DESTDIR=$RPM_BUILD_ROOT %{make_args} 

# --------------------------------------------------------------------
# Installation scripts.  The scripts are passed as the first argument,
# the number of packages currently installed.  That is
#
#          |  1st time  |  upgrade  | remove
#  --------+------------+-----------+--------
#  pre     |     1      |     2     |   -
#  post    |     1      |     2     |   -
#  preun   |     -      |     1     |   0
#  postun  |     -      |     1     |   0      
#
# Post installation script
%post

# remove old log and run file
if test -f /var/log/@PACKAGE@d.log ; then rm /var/log/@PACKAGE@d.log ; fi
if test -f /var/run/@PACKAGE@d.pid ; then rm /var/run/@PACKAGE@d.pid ; fi
# Remove old log and run directories
if test -d /var/log/@PACKAGE@ ; then rm -rf /var/log/@PACKAGE@ ; fi
if test -d /var/run/@PACKAGE@ ; then rm -rf /var/run/@PACKAGE@ ; fi
# make new log and run directory
if test -d /var/log/@PACKAGE@d ; then chown -R fmd:fmd /var/log/@PACKAGE@d ; fi
if test -d /var/run/@PACKAGE@d ; then chown -R fmd:fmd /var/run/@PACKAGE@d ; fi

# Change owner of log and run file 
if test -f /var/log/@PACKAGE@d/@PACKAGE@d.log ; then 
    chown fmd:fmd /var/log/@PACKAGE@d/@PACKAGE@d.log
fi
if test -f /var/run/@PACKAGE@d/@PACKAGE@d.pid ; then 
    chown fmd:fmd /var/run/@PACKAGE@d/@PACKAGE@d.pid
fi

if [ "$1" = 1 ]; then
  # first installation
  /sbin/chkconfig --add @PACKAGE@d
  /sbin/service @PACKAGE@d start
fi

# Pre-deinstallation script
%preun
if [ "$1" = 0 ]; then
  # last remove
  /sbin/service @PACKAGE@d stop # >/dev/null 2>&1
  /sbin/chkconfig --del @PACKAGE@d
fi

# Post-deinstallation script
%postun
if [ "$1" -ge "1" ]; then
  # Upgrade
  /sbin/service @PACKAGE@d restart # >/dev/null 2>&1
fi

# --------------------------------------------------------------------
%changelog
* Fri Oct 12 2007 Forward Multiplicity Detector <fmd@fmddaq.int.nbi.dk> 0.4-1
- Initial packaging of FMD Pedestal uploader bundle

