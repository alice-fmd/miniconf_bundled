#
#
#
PACKAGE_NAME	= FMD configruator 
PACKAGE		= miniconf
VERSION		= 1.8
RELEASE		= 3
DISTNAME	= $(PACKAGE)_bundled
DIM_V		= 19.11
CONFDAEMON_V	= 1.2
RCUXX_V		= 1.8
NJOBS		= 
TEMPLATE	:= tmp-XXXXXX
TMP_INST	:= $(PWD)/tmp
TMP_DESTDIR	=  
TMP_PREFIX	=  $(TMP_INST)
PREFIX		= /usr
DESTDIR		=  $(PWD)
JOBS		=  -j
BASE_CONF	=  --disable-shared --mandir=\$${datadir}/man \
		   --localstatedir=/var
MORE_CONF	= 
CONF_ARGS	= $(BASE_CONF) $(MORE_CONF)
MAKE_ARGS 	= 
PACKS		:= dim-$(DIM_V)			\
		   confdaemon-$(CONFDAEMON_V)	\
		   rcuxx-$(RCUXX_V)		\
		   $(PACKAGE)-$(VERSION)	

%.done:%.tar.gz
	@echo -n "Extracting $* sources from $< ..."
	@tar -xzf $<
	@echo "done"
	@echo -n "Building $* ..."
	@(cd $* && 						\
	  set -e &&                                             \
	  echo "TMP_PREFIX=$(TMP_PREFIX)" ;			\
	  echo "TMP_INST=$(TMP_INST)" ;				\
	  echo "TMP_DESTDIR=$(TMP_DESTDIR)" ;			\
	  echo "CONF_ARGS=$(CONF_ARGS)" ;			\
	  ./configure 						\
		--prefix=$(TMP_PREFIX) 				\
		$(CONF_ARGS) 					\
		PATH=$(TMP_INST)/bin:$(TMP_PREFIX)/bin:${PATH}	\
	  &&							\
	  $(MAKE) $(MAKE_ARGS) $(JOBS)				\
	  &&							\
	  echo "Installing $@ in $(TMP_PREFIX)"			\
	  &&							\
	  $(MAKE) install DESTDIR=$(TMP_DESTDIR)) 
	@echo done 
	@touch $@ 

%.tar.gz:
	@set -e ; \
	d=`basename $@ | sed 's/-.*//'` ; \
	if test ! -d ../$$d ; then \
	  echo "Directory ../$$d does not exist" ; exit 1 ; fi ; \
	echo "Making $@ ... "  ; \
	(cd ../$$d && test ! -f configure && autoreconf -i -f) ; \
	(cd ../$$d && test ! -f Makefile && ./configure ) ; \
	(cd ../$$d && $(MAKE) dist >/dev/null) ; \
	cp ../$$d/$@ $@ 

%.spec:%.spec.in Makefile
	sed -e 's/@RPM_VERSION@/$(VERSION)/g'		\
	    -e 's/@VERSION@/$(VERSION)/g'		\
	    -e 's/@RELEASE@/$(RELEASE)/g'		\
	    -e 's/@PACKAGE@/$(PACKAGE)/g'		\
	    -e 's/@PACKAGE_NAME@/$(PACKAGE_NAME)/g'	\
		< $< > $@

all:	$(PACKS:%=%.done)

clean:
	rm -rf  $(PACKS) 		\
		$(PACKS:%=%.done) 	\
		$(PACKS:%=%.log) 	\
		$(DESTDIR)$(PREFIX)	\
		$(PACKAGE).spec 	\
		*~ 

dist: $(PACKS:%=%.tar.gz) $(PACKAGE).spec Makefile
	mkdir $(DISTNAME)-$(VERSION)
	cp $^ $(DISTNAME)-$(VERSION)
	tar -czvf $(DISTNAME)-$(VERSION).tar.gz $(DISTNAME)-$(VERSION)
	rm -rf $(DISTNAME)-$(VERSION)

$(PACKAGE)-%.done:override TMP_PREFIX = $(PREFIX)
$(PACKAGE)-%.done:override TMP_DESTDIR = $(DESTDIR)
$(PACKAGE)-%.done:override MAKE_ARGS = LDFLAGS=-static
$(PACKAGE)-%.done:override MORE_CONF = --with-default-dir=\$${sysconfdir}/sysconfig  --sysconfdir=/etc
dim-%.done:	  override MORE_CONF = --disable-java --disable-jni
rcuxx-%.done:	  override MORE_CONF = --disable-u2f  --disable-fed --disable-documentation --with-fmd --without-feeclient --without-altrocc 

update:$(PACKS:%=%.tar.gz)

distclean: clean
	rm -rf $(DISTNAME)-$(VERSION).tar.gz
	rm -rf $(PACKS) $(PACKS:%=%.tar.gz)
	rm -rf $(TMP_INST)

distcheck: dist
	tar -xzvf $(DISTNAME)-$(VERSION).tar.gz 
	(cd $(DISTNAME)-$(VERSION) && make)
	rm -rf $(DISTNAME)-$(VERSION)
	@echo "==============================================="
	@echo " $(DISTNAME)-$(VERSION).tar.gz  is ready"
	@echo "==============================================="

upload: dist
	scp $(DISTNAME)-$(VERSION).tar.gz lxplus.cern.ch:~/

daq-rpm:	dist
	. /date/setup.sh 
	rpmbuild -ta -D'variant daq' $(DISTNAME)-$(VERSION).tar.gz
rpm:	dist
	rpmbuild -ta  $(DISTNAME)-$(VERSION).tar.gz
#
# EOF
#
